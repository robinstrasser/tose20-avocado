package ch.briggen.bfh.sprklist.web.charts;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.googlecode.wickedcharts.highcharts.jackson.JsonRenderer;
import com.googlecode.wickedcharts.highcharts.options.ChartOptions;
import com.googlecode.wickedcharts.highcharts.options.Options;
import com.googlecode.wickedcharts.highcharts.options.SeriesType;
import com.googlecode.wickedcharts.highcharts.options.Title;
import com.googlecode.wickedcharts.highcharts.options.series.Point;
import com.googlecode.wickedcharts.highcharts.options.series.PointSeries;

import ch.briggen.bfh.sparklist.domain.Rating;
import ch.briggen.bfh.sparklist.domain.RatingRepository;
import spark.Request;
import spark.Response;
import spark.Route;

/**
 * WWW-Controller Liefert einen Pie-Chart für die Bewertungen
 * 
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung
 * !!!
 * 
 * @author T. Lustenberger / abgeleitet von M.Briggen
 *
 */
public class RatingPieChartController implements Route {

	private final Logger log = LoggerFactory.getLogger(RatingPieChartController.class);

	RatingRepository repository = new RatingRepository();

	private static final String MIME_TYPE = "application/json";

	/**
	 * See 
	 * http://www.highcharts.com/docs
	 * http://thombergs.github.io/wicked-charts/apidocs/
	 * https://github.com/thombergs/wicked-charts
	 */
	@Override
	public Object handle(Request request, Response response) throws Exception {
		log.trace("GET / PieChart Ratings");
		response.type(MIME_TYPE);

		Collection<Rating> ratings = repository.getAll();

		Options options = new Options();
		options.setTitle(new Title("Bewertungen"));
		
		ChartOptions co = new ChartOptions();
		co.setType(SeriesType.PIE);
		co.setHeight(600).setWidth(600);
		options.setChartOptions(co);
		
		PointSeries series = new PointSeries();
		series.setName("Bewertung");

		for (Rating i : ratings) {
			series.addPoint(new Point("Bewertung: " + i.getRating(),i.getCount()));
		}

		options.addSeries(series);

		return new JsonRenderer().toJson(options);
	}

}
