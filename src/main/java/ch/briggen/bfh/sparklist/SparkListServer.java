package ch.briggen.bfh.sparklist;

import static spark.Spark.get;
import static spark.Spark.post;

import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.web.ImageEditController;
import ch.briggen.bfh.sparklist.web.ImpressumManagementRootController;
import ch.briggen.bfh.sparklist.web.ItemDeleteController;
import ch.briggen.bfh.sparklist.web.ItemEditController;
import ch.briggen.bfh.sparklist.web.ItemNewController;
import ch.briggen.bfh.sparklist.web.ItemUpdateController;
import ch.briggen.bfh.sparklist.web.ListManagementRootController;
import ch.briggen.bfh.sparklist.web.UserDeleteController;
import ch.briggen.bfh.sparklist.web.UserEditController;
import ch.briggen.bfh.sparklist.web.UserManagementRootController;
import ch.briggen.bfh.sparklist.web.UserNewController;
import ch.briggen.bfh.sparklist.web.UserUpdateController;
import ch.briggen.bfh.sparklist.web.files.PostFileController;
import ch.briggen.bfh.sprklist.web.charts.RatingPieChartController;
import ch.briggen.sparkbase.H2SparkApp;
import ch.briggen.sparkbase.UTF8ThymeleafTemplateEngine;

public class SparkListServer extends H2SparkApp {

	final static Logger log = LoggerFactory.getLogger(SparkListServer.class);

	public static void main(String[] args) {
		
		for(Entry<Object, Object> property: System.getProperties().entrySet()) {
			log.debug(String.format("Property %s : %s", property.getKey(),property.getValue()));
		}

		SparkListServer server = new SparkListServer();
		server.configure();
		server.run();
	}

	@Override
	protected void doConfigureHttpHandlers() {
	
		// Root
		get("/", new ListManagementRootController(), new UTF8ThymeleafTemplateEngine());
		
		// Item
		get("/item", new ItemEditController(), new UTF8ThymeleafTemplateEngine());
		post("/item/update", new ItemUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/item/delete", new ItemDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/item/new", new ItemNewController(), new UTF8ThymeleafTemplateEngine());

		// User
		get("/userList", new UserManagementRootController(), new UTF8ThymeleafTemplateEngine());
		get("/user", new UserEditController(), new UTF8ThymeleafTemplateEngine());
		post("/user/update", new UserUpdateController(), new UTF8ThymeleafTemplateEngine());
		post("/user/new", new UserNewController(), new UTF8ThymeleafTemplateEngine());
		get("/user/delete", new UserDeleteController(), new UTF8ThymeleafTemplateEngine());
		
		// Image
		get("/image", new ImageEditController(), new UTF8ThymeleafTemplateEngine());
    	post("/files/upload", new PostFileController());
		
		// Impressum / Rating
		get("/impressum", new ImpressumManagementRootController(), new UTF8ThymeleafTemplateEngine());
		post("/impressum", new ImpressumManagementRootController(), new UTF8ThymeleafTemplateEngine());
		get("/json/listPieChart", new RatingPieChartController());
	}
}
