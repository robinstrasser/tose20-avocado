package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.getConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RatingRepository {
	
	private final Logger log = LoggerFactory.getLogger(RatingRepository.class);

	/**
	 * Speichert eine neue Bewertung in der DB. INSERT
	 * @param i neu zu erstellende Bewertung
	 */
	public long insert(long rating) {
		log.trace("insert " + rating);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into rating (rating) values (?)", PreparedStatement.RETURN_GENERATED_KEYS);
			stmt.setLong(1, rating);
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long id = key.getLong(1);
			
			return id;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while inserting item " + rating;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
	}
	
	/**
	 * Gibt den Durchschnittswert aller Bewertungen zurück.
	 */
	public double getAverageRating() {
		log.trace("getAverageRating ");
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("SELECT ROUND(avg(rating),2) result FROM RATING ");
			ResultSet rs = stmt.executeQuery();
			rs.next();
			return rs.getDouble("result");
		}
		catch(SQLException e)
		{
			String msg = "SQL error while getAverageRating  " ;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
	}
	
	/**
	 * Liefert alle ratings in der Datenbank
	 * @return Collection aller Items
	 */
	public Collection<Rating> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("SELECT COUNT(Id) Count, Rating FROM rating GROUP BY rating");
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all items. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	

	private static Collection<Rating> mapItems(ResultSet rs) throws SQLException 
	{
		LinkedList<Rating> list = new LinkedList<Rating>();
		long count = 1;
		while(rs.next())
		{
			Rating i = new Rating(count, rs.getDouble("Rating"),rs.getLong("Count"));
			list.add(i);
			count++;
		}
		return list;
	}
}
