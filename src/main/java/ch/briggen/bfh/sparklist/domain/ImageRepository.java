package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.getConnection;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Base64;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import spark.resource.ClassPathResource;

public class ImageRepository {
	private final Logger log = LoggerFactory.getLogger(ImageRepository.class);

	/**
	 * Liefert das Item mit der übergebenen Id
	 * @param id id des Item
	 * @return Item oder NULL
	 * @throws IOException 
	 */
	public Collection<Image> getById(String id) throws IOException {
		LinkedList<Image> list = new LinkedList<Image>();
		log.trace("getById " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, name, image from images where itemid=?");
			stmt.setLong(1, Long.valueOf(id));
			ResultSet rs = stmt.executeQuery();
			
			while (rs.next()) {
				 byte[] encoded = Base64.getEncoder().encode(rs.getBinaryStream("image").readAllBytes());
		         String encodedString = new String(encoded,StandardCharsets.US_ASCII);
		         Image i = new Image(rs.getLong("id"),rs.getString("name"),encodedString);
				list.add(i);
			}
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving items by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
		return list;				
	}
	
	//TODO: Möglichkeit entwickeln, für Anwender Bilder hochzuladen! Momentan nur für Demo Bilder in DB abspeichern...
	public void prepareDemo(String name, String path, String itemId) throws IOException {
		log.trace("Demo purpose only..." );
		
		try(Connection conn = getConnection())
		{
			    PreparedStatement statement = conn.prepareStatement("SELECT * FROM Images where name='"+ name + "'");
			    ResultSet rs = statement.executeQuery();
			    if(rs.next()) {
			    	return;
			    }
		}
		catch(SQLException e)
		{
			String msg = "SQL error while inserting item ";
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
		
		try(Connection conn = getConnection())
		{
				File image = new ClassPathResource(path).getFile();
			    FileInputStream inputStream = new FileInputStream(image);
			    PreparedStatement statement = conn.prepareStatement("insert into images(name, itemid, image) " + "values(?,?,?)");
			    statement.setString(1, name);
			    statement.setString(2, itemId);
			    statement.setBinaryStream(3, (InputStream) inputStream, (int)(image.length()));
			    statement.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while inserting item ";
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
	}
	
		public void saveImage(String name, String itemId, byte [] buffer) throws IOException {
			
			try(Connection conn = getConnection())
			{
				    PreparedStatement statement = conn.prepareStatement("insert into images(name, itemid, image) " + "values(?,?,?)");
				    statement.setString(1, name);
				    statement.setString(2, itemId);
				    statement.setBytes(3, buffer);
				    statement.executeUpdate();
			}
			catch(SQLException e)
			{
				String msg = "SQL error while inserting item ";
				log.error(msg , e);
				throw new RepositoryException(msg);
			}
		}

}
