package ch.briggen.bfh.sparklist.domain;


/**
 * Einzelner User mit einem Namen, E-Mail Adresse und einer Telefon-/Handy-Nr. (und einer eindeutigen Id)
 * @author Timo Lustenberger
 *
 */
public class User {
	
	private long id;
	private String name;	
	private String email;
	private String number;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public User()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param name Username
	 * @param email E-Mail Adresse
	 * @param number Nummer
	 */
	public User(long id, String name, String email, String number)
	{
		this.id = id;
		this.name = name;
		this.email = email;
		this.number = number;
	}

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getMail() {
		return email;
	}

	public void setMail(String email) {
		this.email = email;
	}
	
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}
	
	@Override
	public String toString() {
		return String.format("User:{id: %d; name: %s; email: %s; number: %s}", id, name, email, number);
	}
	

}
