package ch.briggen.bfh.sparklist.domain;

/**
 * Tabelle für die Rating-Daten
 * @author Timo Lustenberger
 *
 */
public class Rating {
	
	private long id;
	private long count;
	private double rating;		
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Rating()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param rating Bewertung
	 */
	public Rating(long id, double rating )
	{
		this.id = id;
		this.rating = rating;
		this.count = 0;
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param rating Bewertung
	 * @param count Anzahl der Bewertungen 
	 */
	public Rating(long id, double rating, long count)
	{
		this.id = id;
		this.rating = rating;
		this.count = count;
	}

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public double getRating() {
		return rating;
	}
	
	public void setRating(double rating) {
		this.rating = rating;
	}

	public long getCount() {
		return count;
	}
	
	public void setCount(long count) {
		this.count = count;
	}
	
	@Override 
	public String toString() {
		return String.format("Rating:{id: %d; rating: %f}", id, rating);
	}
	
}
