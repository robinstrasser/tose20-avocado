package ch.briggen.bfh.sparklist.domain;

/**
 * Tabelle für die Bild-Daten
 * @author Timo Lustenberger
 *
 */
public class Image {
	
	private long id;
	private String name;	
	private String image;	
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Image()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param name Username
	 * @param image Bildaten
	 */
	public Image(long id, String name, String image)
	{
		this.id = id;
		this.name = name;
		this.image = image;
	}

	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getImage() {
		return image;
		}

	public void setImage(String image) {
		this.image = image;
	}
	
	@Override 
	public String toString() {
		return String.format("User:{id: %d; name: %s, image: %s}", id, name, image);
	}
	
}
