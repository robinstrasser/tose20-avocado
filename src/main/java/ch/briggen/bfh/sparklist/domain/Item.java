package ch.briggen.bfh.sparklist.domain;


/**
 * Einzelner Eintrag in der Liste mit Name und Menge (und einer eindeutigen Id)
 * @author Marcel Briggen
 *
 */
public class Item {
	
	private long id;
	private String name;
	private int quantity;
	private long price;
	private int flatSize;//Wohnungsfläche
	private int plz; //postleitzahl
	private String street; //Strasse
	private long userid; //User ID
	
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Item()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param name Name des Eintrags in der Liste
	 * @param quantity Menge
	 * @param price Preis für die Wohnung im Monat
	 * @param flatSize Grösse der Wohnung in m^2
	 * @param plt Postleitzahl der Wohnung
	 * @param String street Adresse der Wohnung
	 * @param userId des Erstellers der Wohnung
	 */
	public Item(long id, String name, int quantity, long price, int flatSize, int plz, String street, long userid)
	{
		this.id = id;
		this.name = name;
		this.quantity =quantity;
		this.price = price;
		this.flatSize=flatSize;
		this.plz=plz;
		this.street=street;	
		this.userid=userid;
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param name Name des Eintrags in der Liste
	 * @param quantity Menge
	 * @param price Preis für die Wohnung im Monat
	 * @param flatSize Grösse der Wohnung in m^2
	 * @param plt Postleitzahl der Wohnung
	 * @param String street Adresse der Wohnung
	 */
	public Item(long id, String name, int quantity, long price, int flatSize, int plz, String street)
	{
		this.id = id;
		this.name = name;
		this.quantity =quantity;
		this.price = price;
		this.flatSize=flatSize;
		this.plz=plz;
		this.street=street;	
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public long getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	public int getFlatSize() {
		return flatSize;
	}
	
	public void setFlatSize(int flatSize) {
		this.flatSize = flatSize;
	}
	public int getPlz() {
		return plz;
	}
	
	public void setPlz(int plz) {
		this.plz = plz;
	}
	public String getStreet() {
		return street;
	}
	
	public void setStreet(String street) {
		this.street = street;
	}
	
	public long getUserId() {
		return userid;
	}
	
	public void setUserId(long userid) {
		this.userid = userid;
	}
	
	@Override
	public String toString() {
		return String.format("Item:{id: %d; name: %s; quantity: %d; price: %d; flatSize: %d; plz: %d; street: %s; id: %d}", id, name, quantity, price, flatSize, plz, street, userid);
	}
	

}
