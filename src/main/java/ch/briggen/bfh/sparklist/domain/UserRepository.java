package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.getConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Repository für alle User. 
 * Hier werden alle Funktionen für die DB-Operationen zu den Usern implementiert
 * @author Timo Lustenberger
 *
 */
public class UserRepository {
	private final Logger log = LoggerFactory.getLogger(UserRepository.class);
	
	/**
	 * Liefert alle User in der Datenbank mit Join auf Kontakt-Daten
	 * @return Collection aller User
	 */
	public Collection<User> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select user.id, user.name, contact.email, contact.number from user "
					+ "left join contact on contact.userid=user.id");
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all users. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	/**
	 * Liefert den User mit der übergebenen Id
	 * @param id id des Item
	 * @return Item oder NULL
	 */
	public User getById(long id) {
		log.trace("getById " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select user.id, user.name, contact.email, contact.number from user "
					+ "left join contact on contact.userid=user.id "
					+ "where user.id=?");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapItems(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving items by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}
	
	
	/**
	 * Speichert den übergebenen User in der Datenbank. UPDATE.
	 * @param i
	 */
	public void save(User i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update user set name=? where id=?");
			stmt.setString(1, i.getName());
			stmt.setLong(2, i.getId());
			
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating user " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	 * Löscht den User mit der angegebenen Id von der DB
	 * @param id Item ID
	 */
	public void delete(long id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt1 = conn.prepareStatement("update items set items.userid=NULL where items.userid=?");
			stmt1.setLong(1, id);
			stmt1.executeUpdate();
			
			PreparedStatement stmt2 = conn.prepareStatement("delete from contact where userid=?");
			stmt2.setLong(1, id);
			stmt2.executeUpdate();
			
			PreparedStatement stmt3 = conn.prepareStatement("delete from user where id=?");
			stmt3.setLong(1, id);
			stmt3.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing user by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	/**
	 * Speichert den angegebenen User in der DB. INSERT.
	 * @param i neu zu erstellender User
	 * @return Liefert die von der DB generierte id des neuen Users zurück
	 */
	public long insert(User i) {
		
		log.trace("insert " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into user (name) values (?)", PreparedStatement.RETURN_GENERATED_KEYS);
			stmt.setString(1, i.getName());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long id = key.getLong(1);
			
			PreparedStatement stmt2 = conn.prepareStatement("insert into contact (email,number,userid) values (?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
			stmt2.setString(1, i.getMail());
			stmt2.setString(2, i.getNumber());
			stmt2.setLong(3, id);
			stmt2.executeUpdate();
			return id;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while inserting item " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Helper zum konvertieren der Resultsets in User-Objekte. Siehe getByXXX Methoden.
	 * @author Marcel Briggen
	 * @throws SQLException 
	 *
	 */
	private static Collection<User> mapItems(ResultSet rs) throws SQLException 
	{
		LinkedList<User> list = new LinkedList<User>();
		while(rs.next())
		{
			User i = new User(rs.getLong("id"),rs.getString("name"),rs.getString("email"),rs.getString("number"));
			list.add(i);
		}
		return list;
	}

}
