package ch.briggen.bfh.sparklist.web;

import java.io.IOException;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ImageRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Fotos
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Timo Lustenberger
 *
 */

public class ImageEditController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(ImageEditController.class);
	private ImageRepository itemRepo = new ImageRepository();
	
	/**
	 * Requesthandler zum Anzeigen eines Fotos. 
	 * Hört auf GET /image
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "imageListTemplate" .
	 */
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		prepareDemo();
		
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("postAction", "/files/upload?id="+idString);
		
		//TODO: check if 0 or null
		if(null == idString) {
			log.trace("No item parameter found!");
			// return
			response.redirect("/");
		} else	{
		log.trace("GET /image für Anzeige mit id des Items:  " + idString);
		model.put("imageDetail", itemRepo.getById(idString));
		}
		
		return new ModelAndView(model, "imageListTemplate");		
	}

	/**
	 * Diese Methode existiert nur, bis ein Upload von Bilder der User implementiert wurde.
	 * Nur für DEMO-Zwecke gedacht...
	 */
	private void prepareDemo() throws IOException {
		String name = "Zimmer innen";
		String path = "/static/pictures/wg1.jpg";
		itemRepo.prepareDemo(name, path, "1");
		
		name = "Gemeinschaftszimmer";
		path = "/static/pictures/wg2.jpg";
		itemRepo.prepareDemo(name, path, "1");
		
		name = "Wohnzimmer";
		path = "/static/pictures/wg3.jpg";
		itemRepo.prepareDemo(name, path, "2");
		
		name = "Zimmer";
		path = "/static/pictures/wg4.jpg";
		itemRepo.prepareDemo(name, path, "2");
		
		name = "Küche";
		path = "/static/pictures/wg5.jpg";
		itemRepo.prepareDemo(name, path, "2");
	}
	
}


