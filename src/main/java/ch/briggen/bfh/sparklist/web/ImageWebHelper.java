package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Image;
import spark.Request;

class ImageWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(ImageWebHelper.class);
	
	public static Image itemFromWeb(Request request)
	{
		return new Image(
				Long.parseLong(request.queryParams("imageDetail.id")),
				request.queryParams("imageDetail.name"),
				request.queryParams("imageDetail.image"));
	}

}
