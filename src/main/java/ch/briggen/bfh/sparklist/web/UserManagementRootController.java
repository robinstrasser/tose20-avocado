package ch.briggen.bfh.sparklist.web;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * WWW-Controller
 * Liefert unter "/userList" die ganze Liste
 * 
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * 
 * @author Timo Lustenberger
 *
 */
public class UserManagementRootController implements TemplateViewRoute {

	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(UserManagementRootController.class);

	UserRepository repository = new UserRepository();

	/**
	 *Liefert die Liste als Root-Seite für die Benutzer "/userList" zurück 
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		//User werden geladen und die Collection dann für das Template unter dem namen "userList" bereitgestellt
		//Das Template muss dann auch den Namen "userList" verwenden.
		HashMap<String, Collection<User>> model = new HashMap<String, Collection<User>>();
		model.put("userList", repository.getAll());
				
		return new ModelAndView(model, "userListTemplate");
	}
	
}
