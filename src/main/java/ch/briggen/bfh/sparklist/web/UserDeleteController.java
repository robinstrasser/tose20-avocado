package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen User
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Timo Lustenberger
 *
 */
public class UserDeleteController implements TemplateViewRoute {	
	private final Logger log = LoggerFactory.getLogger(UserDeleteController.class);	
	private UserRepository userRepo = new UserRepository();

	/**
	 * Löscht das Item mit der übergebenen id in der Datenbank
	 * /item/delete&id=987 löscht das Item mit der Id 987 aus der Datenbank
	 * 
	 * @return Redirect zurück zur Liste
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String id = request.queryParams("id");
		log.trace("GET /user/delete mit id " + id);
		
		Long longId = Long.parseLong(id);
		userRepo.delete(longId);
		response.redirect("/userList");
		return null;
	}
}


