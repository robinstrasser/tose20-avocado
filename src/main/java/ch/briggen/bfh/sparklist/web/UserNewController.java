package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen User
 * @author Timo Lustenberger
 *
 */

public class UserNewController implements TemplateViewRoute {
	private final Logger log = LoggerFactory.getLogger(UserNewController.class);
	private UserRepository userRepo = new UserRepository();
	
	/**
	 * Erstellt einen neuen Benutzer in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /item&id=99 wenn die id 99 war.)
	 * 
	 * Hört auf POST /user/new"
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		User userDetail = UserWebHelper.userFromWeb(request);
		log.trace("POST /user/new mit userDetail " + userDetail);
		
		userRepo.insert(userDetail);
		
		// nach dem Hinzufügen wird wieder auf die Übersicht redirected
		response.redirect("/userList");
		return null;
	}
}


