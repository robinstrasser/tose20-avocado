package ch.briggen.bfh.sparklist.web.files;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletException;
import javax.servlet.http.Part;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.ImageRepository;
import spark.Request;
import spark.Response;
import spark.Route;

public class PostFileController implements Route {
	
	private final Logger log = LoggerFactory.getLogger(PostFileController.class);
	private ImageRepository imageRepo = new ImageRepository();
	
	@Override
	public Object handle(Request request, Response response) throws Exception {
		log.trace("POST /files/upload");
		
		//change request parsing to multipart and set buffer directory
		request.attribute("org.eclipse.jetty.multipartConfig", new MultipartConfigElement("./uploads_temp"));
	
		logPartsInfos(request);
		
		String idString = request.queryParams("id");
		
		Part filePart = request.raw().getPart("uploadedfile");
		String contentType = filePart.getContentType();
		String filename = filePart.getSubmittedFileName().replace(".png", "").replace(".jpg", "");
		
		InputStream is = request.raw().getPart(filePart.getName()).getInputStream();
		byte [] buffer = new byte[(int)filePart.getSize()]; //TODO remove risky cast...
		log.debug("\tBytes read from input stream: "+ is.read(buffer)); //TODO error handling, read until end
		
		//return the uploaded content
		response.header("content-type", contentType);
		response.status(200);
		
		
		imageRepo.saveImage(filename, idString, buffer);
		
		response.redirect("/image?id="+idString);
		return null;
	}

	private void logPartsInfos(Request request) throws IOException, ServletException {
		for(Part p : request.raw().getParts())
		{
			log.debug(String.format("\tRequestpart name: %s, contentType: %s, submittedFileName %s, size: %s ",
					p.getName(),
					p.getContentType(),
					p.getSubmittedFileName(),
					p.getSize()));
			
			for(String s : p.getHeaderNames())
			{
				log.debug(String.format("\t\theader: %s --> \t %s", s, p.getHeader(s)));
			}
		}
	}

}
