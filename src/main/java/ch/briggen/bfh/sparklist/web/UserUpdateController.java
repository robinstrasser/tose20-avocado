package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Usern
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Timo Lustenberger
 */
public class UserUpdateController implements TemplateViewRoute  {
	private final Logger log = LoggerFactory.getLogger(UserUpdateController.class);
	private UserRepository userRepo = new UserRepository();
	
	/**
	 * Schreibt den geänderten User zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/userList) mit der Item-id als Parameter mit dem namen id.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /user/update
	 * 
	 * @return redirect nach /userList
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		User userDetail = UserWebHelper.userFromWeb(request);
		
		log.trace("POST /user/update mit itemDetail " + userDetail);
		
		//Speichern des Items in dann den Parameter für den Redirect abfüllen
		//der Redirect erfolgt dann z.B. auf /user&id=3 (wenn userDetail.getId == 3 war)
		userRepo.save(userDetail);
		response.redirect("/userList");
		return null;
	}
}


