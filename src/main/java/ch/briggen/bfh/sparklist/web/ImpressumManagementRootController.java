package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.RatingRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * WWW-Controller
 * Liefert unter "/impressum" die ganze Liste
 * 
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * 
 * @author Timo Lustenberger
 *
 */
public class ImpressumManagementRootController implements TemplateViewRoute {

	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(ImpressumManagementRootController.class);
	RatingRepository repository = new RatingRepository ();
	
	/**
	 *Liefert die Liste als Root-Seite für die Benutzer "/impressum" zurück 
	 */	
	@Override 
	public ModelAndView handle(Request request, Response response) throws Exception {
		HashMap<String, Object> model = new HashMap<String, Object>();
		String rate = request.queryParams("rate");
		
		if(null != rate) {
			repository.insert(Long.valueOf(rate));
		}	
		
		String avgRate = String.valueOf(repository.getAverageRating());
		model.put("rating", avgRate);
		return new ModelAndView(model, "impressumTemplate");
	}
	 
}
