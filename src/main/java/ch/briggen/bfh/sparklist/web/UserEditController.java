package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.User;
import ch.briggen.bfh.sparklist.domain.UserRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen User
 * @author Timo Lustenberger
 *
 */

public class UserEditController implements TemplateViewRoute{	
	private final Logger log = LoggerFactory.getLogger(UserEditController.class);
	private UserRepository userRepo = new UserRepository();
	
	
	/**
	 * Requesthandler zum Bearbeiten eines Users. 
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Wenn der id Parameter 0 ist wird beim submitten des Formulars ein neuer User erstellt (Aufruf von /user/new)
	 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars der User mit der übergebenen id upgedated (Aufruf /user/save)
	 * Hört auf GET /user
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "userDetailTemplate" .
	 */
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
				
		if(null == idString || ("0").equals(idString))
		{
			log.trace("GET /user für INSERT mit id " + idString);
			//der Submit-Button ruft /user/new auf --> INSERT
			model.put("postAction", "/user/new");
			model.put("userDetail", new User());
			
		}
		else
		{
			log.trace("GET /user für UPDATE mit id " + idString);
			//der Submit-Button ruft /user/update auf --> UPDATE
			model.put("postAction", "/user/update");
			
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "userDetail" hinzugefügt. itemDetal muss dem im HTML-Template verwendeten Namen entsprechen 
			Long id = Long.parseLong(idString);
			User i = userRepo.getById(id);
			model.put("userDetail", i);
		}
		
		//das Template userDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "userDetailTemplate");
	}
	
	
	
}


