package ch.briggen.bfh.sparklist.web;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Item;
import ch.briggen.bfh.sparklist.domain.ItemRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * WWW-Controller
 * Liefert unter "/" die ganze Liste
 * 
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * 
 * @author M. Briggen
 *
 */
public class ListManagementRootController implements TemplateViewRoute {

	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(ListManagementRootController.class);

	ItemRepository repository = new ItemRepository();
	Request request;

	/**
	 *Liefert die Liste als Root-Seite "/" zurück 
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		this.request = request;
		//Items werden geladen und die Collection dann für das Template unter dem namen "list" bereitgestellt
		//Das Template muss dann auch den Namen "list" verwenden.
		HashMap<String, Collection<Item>> model = new HashMap<String, Collection<Item>>();
		
		// Falls ein Filter für den Preis gesetzt wird, wird dieser im DB-Statement berücksichtigt:		
		if(hasPriceLimit() && hasPriceSorting() && hasDescription()) {
			String description = request.queryMap().value("description");
			String priceLimit = request.queryMap().value("price");
			String priceSorting = request.queryMap().value("priceSorting");
			model.put("list", repository.getByPriceAndDescriptionWithPriceSorting(priceLimit, description, priceSorting));
		} else if(hasPriceLimit() && hasPriceSorting()) {
			String priceLimit = request.queryMap().value("price");
			String priceSorting = request.queryMap().value("priceSorting");
			model.put("list", repository.getByPriceWithPriceSorting(priceLimit, priceSorting));
		} else if(hasPriceSorting() && hasDescription()){
			String description = request.queryMap().value("description");
			String priceSorting = request.queryMap().value("priceSorting");
			model.put("list", repository.getByNameWithPriceSorting(description, priceSorting));
		} else if(hasPriceLimit() && hasDescription()){
			String description = request.queryMap().value("description");
			String priceLimit = request.queryMap().value("price");
			model.put("list", repository.getByPriceAndDescription(priceLimit, description)); 
		} else if(hasPriceLimit()){
			String priceLimit = request.queryMap().value("price");
			model.put("list", repository.getByPrice(priceLimit));
		} else if(hasPriceSorting()){
			String priceSorting = request.queryMap().value("priceSorting");
			model.put("list", repository.getAllWithPriceSorting(priceSorting));
		} else if(hasDescription()) {
			String description = request.queryMap().value("description");
			model.put("list", repository.getByName(description));
		}
		
		if(model.isEmpty()) {
			model.put("list", repository.getAll());
		}
						
		return new ModelAndView(model, "listTemplate");
	}
	
	private boolean hasPriceLimit() {
		boolean hasPriceLimitParam = request.queryMap().hasKey("price");
		if(hasPriceLimitParam) {
			String priceLimit = request.queryMap().value("price");
			if(!priceLimit.contains("No")) {
				return true;
			}
		}
		return false;
	}
	
	private boolean hasPriceSorting() {
		boolean hasPriceSortingParam = request.queryMap().hasKey("priceSorting");
		if(hasPriceSortingParam) {
			String priceSorting = request.queryMap().value("priceSorting");
			if(priceSorting.contains("DESC") || priceSorting.contains("ASC")) {
				return true;
			}
		}
		return false;
	}
	
	private boolean hasDescription() {
		boolean hasDescriptionParam = request.queryMap().hasKey("description");
		if(hasDescriptionParam) {
			String description = request.queryMap().value("description");
			if(!description.isEmpty()) {
				return true;
			}
		}
		return false;
	}
	
	
}
