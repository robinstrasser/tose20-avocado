$(document).ready(function() {
	$("#btnsubmit").click(function() {
		var name= $("#name").val();
		var id= 0;
		var quantity= $("#quantity").val();
		postItem(id,name,quantity,fillForm);
	});
});

function fillForm(itemDetail)
{
	$('#name').val(itemDetail.name);
	$('#id').val(itemDetail.id);
	$('#quantity').val(itemDetail.quantity);
	$('#success-message').show(200);
}


function postItem(itemId, itemName, itemQuantity, successCallback)
{
	callPromise = $.post('/json/item',
						JSON.stringify({
								id : itemId,
								name : itemName,
								quantity : itemQuantity
							}),
						null,
						'json');
	callPromise
		.done(function(data) { fillForm(data)})
		.fail(function() { alert( "error" ); });
}