/* DROP TABLE: */
drop table if exists images;
drop table if exists contact;
drop table if exists items;
drop table if exists user;
drop table if exists rating;

/* CREATE TABLE: */
create table if not exists items (
id long identity, 
name varchar(200), 
quantity decimal, 
price long, 
flatSize integer, 
plz integer, 
street varchar(200),
userid long,  
primary key(id));

create table if not exists user (
id long identity, 
name varchar(200), 
primary key(id));

create table if not exists contact (
id long identity, 
email varchar(200), 
number  varchar(30),
userid long,
primary key(id));

create table if not exists images (
  id long identity NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name varchar(200) NOT NULL,
  itemid long,
  image varbinary);

create table if not exists rating (
  id long identity NOT NULL PRIMARY KEY AUTO_INCREMENT,
  rating double(4)  NOT NULL);

/* Vordefinierte WG-Inserate */
INSERT INTO ITEMS VALUES
(1, 'WG im Marzili', 15, 600, 72, 3005,'Marziliweg 1', 2),
(2, 'WG-Zimmer in der Altstadt', 17, 940 , 50 ,3001, 'Altstadt 12b', 3),
(3, 'Zentrale Party WG', 8, 500, 90 , 3002, 'Brückfeldstrasse 71', 4),
(4, 'Studenten-WG', 22, 750, 66, 3017, 'Seftigenstrasse 41', 5),
(5, 'Zwecks-WG', 16, 820, 150, 6166, 'Bahnhofstr. 1', 2);

/* Vordefinierte User */
INSERT INTO USER VALUES
(1, 'Admin'),
(2, 'Max Meier'),
(3, 'Rolf Müller'),
(4, 'Dario Villiger'),
(5, 'Alain Sutter');

/* Vordefinierte Kontakte zu User */
INSERT INTO CONTACT VALUES
(1, 'admin@wgzimmer.ch', '+41 52 472 28 28',1),
(2, 'max.meier@gmail.com', '+41 79 472 22 28',2),
(3, 'rolf_mueller@gmail.com', '+41 78 232 11 23',3),
(4, 'dario.villiger@bfh.ch', '+41 62 472 28 28',4),
(5, 'alain.sutter1@srf.ch', '+41 41 234 28 28',5);

/* add FOREIGN KEYS*/ 
ALTER TABLE images
ADD FOREIGN KEY (itemid) REFERENCES items(id);

ALTER TABLE contact
ADD FOREIGN KEY (userid) REFERENCES user(id);

ALTER TABLE items
ADD FOREIGN KEY (userid) REFERENCES user(id);