package ch.briggen.bfh.sparklist.domain;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class ItemTest {

	@Test
	void testEmptyContructorYieldsEmptyObject() {
		Item i = new Item();
		assertThat("Id",i.getId(),is(equalTo(0l)));
		assertThat("Name",i.getName(),is(equalTo(null)));
		assertThat("Quantity",i.getQuantity(),is(equalTo(0)));
		assertThat("Price", i.getPrice(),is(equalTo(0l)) );
		assertThat("FlatSize", i.getFlatSize(),is(equalTo(0)) );
		assertThat("PLZ", i.getPlz(),is(equalTo(0)) );
		assertThat("Street", i.getStreet(),is(equalTo(null)) );
		assertThat("UserId", i.getUserId(),is(equalTo(0l)) );
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,15,800,50,6166,Bahnhofstr. 3","2,Two,17,650,48,6162,Brückfeldstrasse 41","3,Three,15,599,55,6210,Römerweg 5"})
	void testContructorAssignsAllFields(long id,String name,int quantity, long price, int flatSize, int plz, String street) {
		Item i = new Item(id, name, quantity, price, flatSize, plz, street);
		assertThat("Id",i.getId(),equalTo(id));
		assertThat("Name", i.getName(),is(name) );
		assertThat("Quantity", i.getQuantity(),is(quantity) );
		assertThat("Price", i.getPrice(),is(price) );
		assertThat("FlatSize", i.getFlatSize(),is(flatSize) );
		assertThat("PLZ", i.getPlz(),is(plz) );
		assertThat("Street", i.getStreet(),is(street) );
		assertThat("UserId", i.getUserId(),is(equalTo(0l)) );
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,15,800,50,6166,Bahnhofstr. 3,2","2,Two,17,650,48,6162,Brückfeldstrasse 41,1","3,Three,15,599,55,6210,Römerweg 5,3"})
	void testSetters(long id,String name,int quantity, long price, int flatSize, int plz, String street, long userId) {
		Item i = new Item();
		i.setId(id);
		i.setName(name);
		i.setQuantity(quantity);
		i.setPrice((int)price);
		i.setFlatSize(flatSize);
		i.setPlz(plz);
		i.setStreet(street);
		i.setUserId(userId);
		assertThat("Id",i.getId(),equalTo(id));
		assertThat("Name", i.getName(),is(name) );
		assertThat("Quantity", i.getQuantity(),is(quantity) );
		assertThat("Price", i.getPrice(),is(price) );
		assertThat("FlatSize", i.getFlatSize(),is(flatSize) );
		assertThat("PLZ", i.getPlz(),is(plz) );
		assertThat("Street", i.getStreet(),is(street) );
		assertThat("UserId", i.getUserId(),is(userId) );
	}
	
	@Test
	void testToString() {
		Item i = new Item();
		System.out.println(i.toString());
		assertThat("toString smoke test",i.toString(),not(nullValue()));
	}
}
