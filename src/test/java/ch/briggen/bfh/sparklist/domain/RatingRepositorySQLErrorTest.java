package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.ItemDBTestHelper.initDataSourceForTest;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RatingRepositorySQLErrorTest {

	RatingRepository repo = null;
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		repo = new RatingRepository();
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testSQLExceptionInsert() {
		assertThrows(RepositoryException.class, ()->{repo.insert(0);});		
	}

	@Test
	void testSQLExceptionGetAverageRating() {
		assertThrows(RepositoryException.class, ()->{repo.getAverageRating();});		
	}
	
}
