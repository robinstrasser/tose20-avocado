package ch.briggen.bfh.sparklist.domain;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class UserTest {

	@Test
	void testEmptyContructorYieldsEmptyObject() {
		User i = new User();
		assertThat("Id",i.getId(),is(equalTo(0l)));
		assertThat("Name",i.getName(),is(equalTo(null)));
		assertThat("E-Mail",i.getMail(),is(equalTo(null)));
		assertThat("Nummer", i.getNumber(),is(equalTo(null)));

	}
	
	@ParameterizedTest
	@CsvSource({"1,Timo Mueller,timo@gmail.com,041495837","2,Marco Ernst,marco.ernst@gmail.com,0792746253","3,Robin Schulz,robin.schulz@gmail.com,0783746253"})
	void testContructorAssignsAllFields(long id,String name, String email, String number) {
		User i = new User(id, name, email, number);
		assertThat("Id",i.getId(),equalTo(id));
		assertThat("Name", i.getName(),is(name));
		assertThat("E-Mail", i.getMail(),is(email));
		assertThat("Nummer", i.getNumber(),is(number));;
	}
	
	@ParameterizedTest
	@CsvSource({"1,Timo Mueller,timo@gmail.com,041495837","2,Marco Ernst,marco.ernst@gmail.com,0792746253","3,Robin Schulz,robin.schulz@gmail.com,0783746253"})
	void testSetters(long id,String name, String email, String number) {
		User i = new User();
		i.setId(id);
		i.setName(name);
		i.setMail(email);
		i.setNumber(number);
		assertThat("Id",i.getId(),equalTo(id));
		assertThat("Name", i.getName(),is(name));
		assertThat("E-Mail", i.getMail(),is(email));
		assertThat("Nummer", i.getNumber(),is(number));;
	}
	
	@Test
	void testToString() {
		User i = new User();
		System.out.println(i.toString());
		assertThat("toString smoke test",i.toString(),not(nullValue()));
	}
}
