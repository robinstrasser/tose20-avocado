package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.ItemDBTestHelper.initDataSourceForTest;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ImageRepositorySQLErrorTest {

	ImageRepository repo = null;
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		repo = new ImageRepository();
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testSQLExceptionGetById() {
		assertThrows(RepositoryException.class, ()->{repo.getById("0");});		
	}
	
	@Test
	void testSQLExceptionSaveImage() {
		assertThrows(RepositoryException.class, ()->{repo.saveImage("DontSave", "NoItem", null);;});		
	}

	
}
