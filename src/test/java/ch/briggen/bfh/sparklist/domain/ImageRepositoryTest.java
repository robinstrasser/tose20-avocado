package ch.briggen.bfh.sparklist.domain;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

import java.io.IOException;
import java.util.Collection;

import static ch.briggen.bfh.sparklist.domain.UserDBTestHelper.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;


class ImageRepositoryTest {

	ImageRepository repo = null;

	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		repo = new ImageRepository();
	}

	
	@ParameterizedTest
	@CsvSource({"TestImage1,1,0123456"})
	void testsaveImage(String name, String itemId, int image) throws IOException
	{
		repo.saveImage(name, itemId, new byte[(int)image]);
		
		Collection<Image> fromDB  = repo.getById(itemId);
		Image imageFromDB = fromDB.iterator().next();
		assertThat("Check name in DB", imageFromDB.getName(),is(name));
	}	
}
