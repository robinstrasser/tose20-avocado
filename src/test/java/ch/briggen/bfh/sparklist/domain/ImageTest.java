package ch.briggen.bfh.sparklist.domain;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class ImageTest {

	@Test
	void testEmptyContructorYieldsEmptyObject() {
		Image i = new Image();
		assertThat("Id",i.getId(),is(equalTo(0l)));
		assertThat("Name",i.getName(),is(equalTo(null)));
		assertThat("Image",i.getImage(),is(equalTo(null)));
	}
	
	@ParameterizedTest
	@CsvSource({"1,Innenansicht,TestPng","2,Aussenansicht,TestJpg","3,Küche,15,TestGif"})
	void testContructorAssignsAllFields(long id,String name, String image) {
		Image i = new Image(id, name, image);
		assertThat("Id",i.getId(),equalTo(id));
		assertThat("Name", i.getName(),is(name) );
		assertThat("Image", i.getImage(),is(image) );
	}
	
	@ParameterizedTest
	@CsvSource({"1,Innenansicht,TestPng","2,Aussenansicht,TestJpg","3,Küche,15,TestGif"})
	void testSetters(long id,String name, String image) {
		Image i = new Image();
		i.setId(id);
		i.setName(name);
		i.setImage(image);
		assertThat("Id",i.getId(),equalTo(id));
		assertThat("Name", i.getName(),is(name) );
		assertThat("Image", i.getImage(),is(image));
	}
	
	@Test
	void testToString() {
		Image i = new Image();
		System.out.println(i.toString());
		assertThat("toString smoke test",i.toString(),not(nullValue()));
	}
}
