package ch.briggen.bfh.sparklist.domain;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

import static ch.briggen.bfh.sparklist.domain.UserDBTestHelper.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;


class UserRepositoryTest {

	UserRepository repo = null;

	private static void populateRepo(UserRepository r) {
		for(int i = 0; i <10; ++i) {
			User dummy = new User(0,"Fake Test Item" + i,"Fake E-Mail" + i ,"Fake number" + i);
			r.insert(dummy);
		}
	}
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		repo = new UserRepository();
	}

	@Test
	void testPreparedDB() {
		assertThat("Five records are prepared while initializing",repo.getAll().size(),is(5));
	}
	
	@Test
	void testPopulatedDB()
	{
		populateRepo(repo);
		assertThat("Freshly populated DB must hold 10 users + 5 prepared",repo.getAll().size(),is(15));
	}
	
	@ParameterizedTest
	@CsvSource({"1,Timo Mueller,timo@gmail.com,041495837","2,Marco Ernst,marco.ernst@gmail.com,0792746253","3,Robin Schulz,robin.schulz@gmail.com,0783746253"})
	void testInsertItems(long id,String name, String email, String number)
	{
		populateRepo(repo);
		User i = new User(id, name, email, number);
		long dbId = repo.insert(i);
		User fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("name = name", fromDB.getName(),is(name));
		assertThat("E-Mail = mail", fromDB.getMail(),is(email));
		assertThat("Nummer = number", fromDB.getNumber(),is(number));
	}	
	
	@Test
	void testDeleteManyRows()
	{
		populateRepo(repo);
		for(User i : repo.getAll())
		{
			repo.delete(i.getId());
		}
		
		assertThat("DB must be empty after deleteing all items",repo.getAll().isEmpty(),is(true));
	}

}
