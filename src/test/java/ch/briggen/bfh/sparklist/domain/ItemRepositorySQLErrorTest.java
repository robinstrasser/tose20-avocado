package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.ItemDBTestHelper.initDataSourceForTest;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ItemRepositorySQLErrorTest {

	ItemRepository repo = null;
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		repo = new ItemRepository();
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testSQLExceptionGetAll() {
		assertThrows(RepositoryException.class, ()->{repo.getAll();});		
	}
	
	@Test
	void testSQLExceptionGetByName() {
		assertThrows(RepositoryException.class, ()->{repo.getByName("NotHere");});		
	}
	
	@Test
	void testSQLExceptionGetById() {
		assertThrows(RepositoryException.class, ()->{repo.getById(0);});		
	}
	
	@Test
	void testSQLExceptionGetByPrice() {
		assertThrows(RepositoryException.class, ()->{repo.getByPrice("maxPrice");});		
	}
	
	@Test
	void testSQLExceptionGetByPriceAndDescription() {
		assertThrows(RepositoryException.class, ()->{repo.getByPriceAndDescription("maxPrice","Test");});		
	}
	
	@Test
	void testSQLExceptionGetByPriceAndDescriptionWithPriceSorting() {
		assertThrows(RepositoryException.class, ()->{repo.getByPriceAndDescriptionWithPriceSorting("maxPrice","Test","DESC");});		
	}
	
	
	@Test
	void testSQLExceptionGetByPriceWithPriceSorting() {
		assertThrows(RepositoryException.class, ()->{repo.getByPriceWithPriceSorting("maxPrice", "DESC");});		
	}
	
	@Test
	void testSQLExceptionGetByNameWithPriceSorting() {
		assertThrows(RepositoryException.class, ()->{repo.getByNameWithPriceSorting("Test", "DESC");});		
	}
	
	@Test
	void testSQLExceptionGetAllWithPriceSorting() {
		assertThrows(RepositoryException.class, ()->{repo.getAllWithPriceSorting("DESC");});		
	}
	
	@Test
	void testSQLExceptionInsert() {
		assertThrows(RepositoryException.class, ()->{repo.insert(null);});		
	}

	@Test
	void testSQLExceptionUpdate() {
		assertThrows(RepositoryException.class, ()->{repo.save(null);});		
	}
	
	@Test
	void testSQLExceptionDelete() {
		assertThrows(RepositoryException.class, ()->{repo.delete(0);});		
	}
}
