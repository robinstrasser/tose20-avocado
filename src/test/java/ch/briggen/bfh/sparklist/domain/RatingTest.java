package ch.briggen.bfh.sparklist.domain;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class RatingTest {

	@Test
	void testEmptyContructorYieldsEmptyObject() {
		Rating i = new Rating();
		assertThat("Id",i.getId(),is(equalTo(0l)));
		assertThat("Rating", i.getRating(),is(equalTo(0.0)) );
		assertThat("Count", i.getCount(),is(equalTo(0l)));
	}
	
	@ParameterizedTest
	@CsvSource({"1,4.0","2,1.0","3,5.0"})
	void testContructorAssignsAllFields(long id, double rating) {
		Rating i = new Rating(id, rating);
		assertThat("Id",i.getId(),equalTo(id));
		assertThat("Rating", i.getRating(),is(rating) );
	}
	
	@ParameterizedTest
	@CsvSource({"1,4.0,2","2,1.0,5","3,5.0,0"})
	void testContructorWithCountAssignsAllFields(long id, double rating, long count) {
		Rating i = new Rating(id, rating, count);
		assertThat("Id",i.getId(),equalTo(id));
		assertThat("Rating", i.getRating(),is(rating) );
		assertThat("Count", i.getCount(),is(count));
	}
	
	@ParameterizedTest
	@CsvSource({"1,4,2","2,1,5","3,5,0"})
	void testSetters(long id, double rating, long count) {
		Rating i = new Rating();
		i.setId(id);
		i.setRating(rating);
		i.setCount(count);

		assertThat("Id",i.getId(),equalTo(id));
		assertThat("Rating", i.getRating(),is(rating) );
		assertThat("Count",i.getCount(),equalTo(count));
	}
	
	@Test
	void testToString() {
		Rating i = new Rating();
		System.out.println(i.toString());
		assertThat("toString smoke test",i.toString(),not(nullValue()));
	}
}
