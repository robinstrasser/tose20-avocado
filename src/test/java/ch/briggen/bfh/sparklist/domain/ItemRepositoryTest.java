package ch.briggen.bfh.sparklist.domain;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import static ch.briggen.bfh.sparklist.domain.ItemDBTestHelper.*;


import java.util.Collection;
import java.util.NoSuchElementException;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;


class ItemRepositoryTest {

	ItemRepository repo = null;

	
	private static void populateRepo(ItemRepository r) {
		for(int i = 0; i <10; ++i) {
			Item dummy = new Item(0,"Fake Test Item" + i, i,i,i,i,"Fake test street +i");
			r.insert(dummy);
		}
	}
	
	@BeforeEach
	void setUp() throws Exception {
		String randomName = "Test@"+System.currentTimeMillis();
		initDataSourceForTest(randomName);
		initDB();
		repo = new ItemRepository();
	}

	@Test
	void testPreparedDB() {
		assertThat("Five records are prepared while initializing",repo.getAll().size(),is(5));
	}
	
	@Test
	void testPopulatedDB()
	{
		populateRepo(repo);
		assertThat("Freshly populated DB must hold 10 items + 5 prepared",repo.getAll().size(),is(15));
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,15,800,50,6166,Bahnhofstr. 3","2,Two,17,650,48,6162,Brückfeldstrasse 41","3,Three,15,599,55,6210,Römerweg 5"})
	void testInsertItems(long id,String name,int quantity, long price, int flatSize, int plz, String street)
	{
		populateRepo(repo);
		Item i = new Item(id, name, quantity, price, flatSize, plz, street);
		long dbId = repo.insert(i);
		Item fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("name = name", fromDB.getName(),is(name) );
		assertThat("quantity = quantity", fromDB.getQuantity(),is(quantity) );
		assertThat("price = price", fromDB.getPrice(),is(price) );
		assertThat("flatSize = flatSize", fromDB.getFlatSize(),is(flatSize) );
		assertThat("plz = plz", fromDB.getPlz(),is(plz) );
		assertThat("street = street", fromDB.getStreet(),is(street) );

	}
	
	@ParameterizedTest
	@CsvSource({"1,One,15,800,50,6166,Bahnhofstr. 3,750","2,Two,17,650,48,6162,Brückfeldstrasse 41,599","3,Three,15,599,55,6210,Römerweg 5,1100"})
	void testUpdateItems_updatePrice(long id,String name,int quantity, long price, int flatSize, int plz, String street, long newPrice)
	{
		populateRepo(repo);
		
		Item i = new Item(id, name, quantity, price, flatSize, plz, street);
		long dbId = repo.insert(i);
		i.setId(dbId);
		i.setPrice((int)newPrice);
		repo.save(i);
		
		Item fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("name = name", fromDB.getName(),is(name) );
		assertThat("quantity = quantity", fromDB.getQuantity(),is(quantity) );
		assertThat("flatSize = flatSize", fromDB.getFlatSize(),is(flatSize) );
		assertThat("plz = plz", fromDB.getPlz(),is(plz) );
		assertThat("street = street", fromDB.getStreet(),is(street) );
		assertThat("price = price", fromDB.getPrice(),is(newPrice) );
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,15,800,50,6166,Bahnhofstr. 3,12","2,Two,17,650,48,6162,Brückfeldstrasse 41,19","3,Three,15,599,55,6210,Römerweg 5,23"})
	void testUpdateItems_updateQuanity(long id,String name,int quantity, long price, int flatSize, int plz, String street, int newQuantity)
	{
		populateRepo(repo);
		
		Item i = new Item(id, name, quantity, price, flatSize, plz, street);
		long dbId = repo.insert(i);
		i.setId(dbId);
		i.setQuantity(newQuantity);
		repo.save(i);
		
		Item fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("name = name", fromDB.getName(),is(name) );
		assertThat("price = price", fromDB.getPrice(),is(price) );
		assertThat("flatSize = flatSize", fromDB.getFlatSize(),is(flatSize) );
		assertThat("plz = plz", fromDB.getPlz(),is(plz) );
		assertThat("street = street", fromDB.getStreet(),is(street) );
		assertThat("quantity = quantity", fromDB.getQuantity(),is(newQuantity) );
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,15,800,50,6166,Bahnhofstr. 3,61","2,Two,17,650,48,6162,Brückfeldstrasse 41,75","3,Three,15,599,55,6210,Römerweg 5,46"})
	void testUpdateItems_updateFlatSize(long id,String name,int quantity, long price, int flatSize, int plz, String street, int newFlatSize)
	{
		populateRepo(repo);
		
		Item i = new Item(id, name, quantity, price, flatSize, plz, street);
		long dbId = repo.insert(i);
		i.setId(dbId);
		i.setFlatSize(newFlatSize);
		repo.save(i);
		
		Item fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("name = name", fromDB.getName(),is(name) );
		assertThat("quantity = quantity", fromDB.getQuantity(),is(quantity) );
		assertThat("price = price", fromDB.getPrice(),is(price) );
		assertThat("plz = plz", fromDB.getPlz(),is(plz) );
		assertThat("street = street", fromDB.getStreet(),is(street) );
		assertThat("flatSize = flatSize", fromDB.getFlatSize(),is(newFlatSize) );
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,15,800,50,6166,Bahnhofstr. 3,3001","2,Two,17,650,48,6162,Brückfeldstrasse 41,3006","3,Three,15,599,55,6210,Römerweg 5,3013"})
	void testUpdateItems_updatePLZ(long id,String name,int quantity, long price, int flatSize, int plz, String street, int newPLZ)
	{
		populateRepo(repo);
		
		Item i = new Item(id, name, quantity, price, flatSize, plz, street);
		long dbId = repo.insert(i);
		i.setId(dbId);
		i.setPlz(newPLZ);
		repo.save(i);
		
		Item fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("name = name", fromDB.getName(),is(name) );
		assertThat("quantity = quantity", fromDB.getQuantity(),is(quantity) );
		assertThat("price = price", fromDB.getPrice(),is(price) );
		assertThat("flatSize = flatSize", fromDB.getFlatSize(),is(flatSize) );
		assertThat("street = street", fromDB.getStreet(),is(street) );
		assertThat("PLZ = PLZ", fromDB.getPlz(),is(newPLZ) );
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,15,800,50,6166,Bahnhofstr. 3,Bahnhofstrasse 3","2,Two,17,650,48,6162,Brückfeldstrasse 41, Brückfeldstr. 41,19","3,Three,15,599,55,6210,Römerweg 5,Seftigenstrasse 40"})
	void testUpdateItems_updateStreet(long id,String name,int quantity, long price, int flatSize, int plz, String street, String newStreet)
	{
		populateRepo(repo);
		
		Item i = new Item(id, name, quantity, price, flatSize, plz, street);
		long dbId = repo.insert(i);
		i.setId(dbId);
		i.setStreet(newStreet);
		repo.save(i);
		
		Item fromDB = repo.getById(dbId);
		assertThat("id = id", fromDB.getId(),is(dbId) );
		assertThat("name = name", fromDB.getName(),is(name) );
		assertThat("quantity = quantity", fromDB.getQuantity(),is(quantity) );
		assertThat("price = price", fromDB.getPrice(),is(price) );
		assertThat("flatSize = flatSize", fromDB.getFlatSize(),is(flatSize) );
		assertThat("plz = plz", fromDB.getPlz(),is(plz) );
		assertThat("street = street", fromDB.getStreet(),is(newStreet) );
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,15,800,50,6166,Bahnhofstr. 3","2,Two,17,650,48,6162,Brückfeldstrasse 41","3,Three,15,599,55,6210,Römerweg 5"})
	void testDeleteItems(long id,String name,int quantity, long price, int flatSize, int plz, String street)
	{
		populateRepo(repo);
		
		Item i = new Item(id, name, quantity, price, flatSize, plz, street);
		long dbId = repo.insert(i);
		Item fromDB = repo.getById(dbId);
		assertThat("Item was written to DB",fromDB,not(nullValue()));
		repo.delete(dbId);
		assertThrows(NoSuchElementException.class, ()->{repo.getById(dbId);},"Item should have been deleted");
	}
	
	@Test
	void testDeleteManyRows()
	{
		populateRepo(repo);
		for(Item i : repo.getAll())
		{
			repo.delete(i.getId());
		}
		
		assertThat("DB must be empty after deleteing all items",repo.getAll().isEmpty(),is(true));
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,15,800,50,6166,Bahnhofstr. 3","2,Two,17,650,48,6162,Brückfeldstrasse 41","3,Three,15,599,55,6210,Römerweg 5"})
	void testGetByName_getOne(long id,String name,int quantity, long price, int flatSize, int plz, String street)
	{
		populateRepo(repo);
		
		Item i = new Item(id, name, quantity, price, flatSize, plz, street);
		repo.insert(i);
		Collection<Item> fromDB = repo.getByName(name);
		assertThat("Exactly one item was returned", fromDB.size(),is(1));
		
		Item elementFromDB = fromDB.iterator().next();
		assertThat("name = name", elementFromDB.getName(),is(name) );
		assertThat("quantity = quantity", elementFromDB.getQuantity(),is(quantity) );
		assertThat("price = price", elementFromDB.getPrice(),is(price) );
		assertThat("flatSize = flatSize", elementFromDB.getFlatSize(),is(flatSize) );
		assertThat("plz = plz", elementFromDB.getPlz(),is(plz) );
		assertThat("street = street", elementFromDB.getStreet(),is(street) );
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,15,800,50,6166,Bahnhofstr. 3","2,Two,17,650,48,6162,Brückfeldstrasse 41","3,Three,15,599,55,6210,Römerweg 5"})
	void testGetByName_getMany(int count,String name,int quantity, long price, int flatSize, int plz, String street)
	{
		populateRepo(repo);
		
		
		for(int n = 0; n < count; ++n) {
			Item i = new Item(0, name, quantity, price, flatSize, plz, street);
			repo.insert(i);
		}
		
		Collection<Item> fromDB = repo.getByName(name);
		assertThat("Exactly one item was returned", fromDB.size(),is(count));
		
		for(Item elementFromDB : fromDB)
		{
			assertThat("name = name", elementFromDB.getName(),is(name) );
			assertThat("quantity = quantity", elementFromDB.getQuantity(),is(quantity) );
			assertThat("price = price", elementFromDB.getPrice(),is(price) );
			assertThat("flatSize = flatSize", elementFromDB.getFlatSize(),is(flatSize) );
			assertThat("plz = plz", elementFromDB.getPlz(),is(plz) );
			assertThat("street = street", elementFromDB.getStreet(),is(street) );
		}
	}
	
	@Test
	void testGetByName_getNoItem()
	{
		populateRepo(repo);
		
		Collection<Item> fromDB = repo.getByName("NotExistingItem");
		assertThat("Exactly no item was returned", fromDB.size(),is(0));
		
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,15,200,50,6166,Bahnhofstr. 3","2,Two,17,300,48,6162,Brückfeldstrasse 41","3,Three,15,400,55,6210,Römerweg 5"})
	void testGetByPrice_getAllItems(long id,String name,int quantity, long price, int flatSize, int plz, String street)
	{
		for(int n = 0; n < id; ++n) {
			Item i = new Item(0, name, quantity, price, flatSize, plz, street);
			repo.insert(i);
		}
		
		Collection<Item> fromDB = repo.getByPrice(String.valueOf(450));
		assertThat("Exactly one item was returned", fromDB.size(),is((int)id));
		
		for(Item elementFromDB : fromDB)
		{
			assertThat("name = name", elementFromDB.getName(),is(name) );
			assertThat("quantity = quantity", elementFromDB.getQuantity(),is(quantity) );
			assertThat("price = price", elementFromDB.getPrice(),is(price) );
			assertThat("flatSize = flatSize", elementFromDB.getFlatSize(),is(flatSize) );
			assertThat("plz = plz", elementFromDB.getPlz(),is(plz) );
			assertThat("street = street", elementFromDB.getStreet(),is(street) );
		}
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,15,450,50,6166,Bahnhofstr. 3"})
	void testGetByPrice_priceIsLTParam_returnItem(long id,String name,int quantity, long price, int flatSize, int plz, String street)
	{
		for(int n = 0; n < id; ++n) {
			Item i = new Item(0, name, quantity, price, flatSize, plz, street);
			repo.insert(i);
		}
		
		Collection<Item> fromDB = repo.getByPrice(String.valueOf(460));
		assertThat("Exactly one item was returned", fromDB.size(),is((int)1));
		
		for(Item elementFromDB : fromDB)
		{
			assertThat("name = name", elementFromDB.getName(),is(name) );
			assertThat("quantity = quantity", elementFromDB.getQuantity(),is(quantity) );
			assertThat("price = price", elementFromDB.getPrice(),is(price) );
			assertThat("flatSize = flatSize", elementFromDB.getFlatSize(),is(flatSize) );
			assertThat("plz = plz", elementFromDB.getPlz(),is(plz) );
			assertThat("street = street", elementFromDB.getStreet(),is(street) );
		}
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,15,490,50,6166,Bahnhofstr. 3"})
	void testGetByPrice_priceIsEQParam_returnItem(long id,String name,int quantity, long price, int flatSize, int plz, String street)
	{

		Item i = new Item(0, name, quantity, price, flatSize, plz, street);
		repo.insert(i);
		
		
		Collection<Item> fromDB = repo.getByPrice(String.valueOf(490));
		assertThat("Exactly one item was returned", fromDB.size(),is((int)1));
		
		for(Item elementFromDB : fromDB)
		{
			assertThat("name = name", elementFromDB.getName(),is(name) );
			assertThat("quantity = quantity", elementFromDB.getQuantity(),is(quantity) );
			assertThat("price = price", elementFromDB.getPrice(),is(price) );
			assertThat("flatSize = flatSize", elementFromDB.getFlatSize(),is(flatSize) );
			assertThat("plz = plz", elementFromDB.getPlz(),is(plz) );
			assertThat("street = street", elementFromDB.getStreet(),is(street) );
		}
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,15,480,50,6166,Bahnhofstr. 3"})
	void testGetByPrice_priceIsGTParam_noItemReturned(long id,String name,int quantity, long price, int flatSize, int plz, String street)
	{
		Item i = new Item(0, name, quantity, price, flatSize, plz, street);
		repo.insert(i);

		Collection<Item> fromDB = repo.getByPrice(String.valueOf(450));
		assertThat("Exactly no item was returned", fromDB.size(),is((int)0));
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,15,800,50,6166,Bahnhofstr. 3","2,Two,17,650,48,6162,Brückfeldstrasse 41","3,Three,15,700,55,6210,Römerweg 5"})
	void testGetAllWithPriceSorting_getAllItems_sortedDESC(long id,String name,int quantity, long price, int flatSize, int plz, String street)
	{
		for(int n = 0; n < id; ++n) {
			Item i = new Item(0, name, quantity, price, flatSize, plz, street);
			repo.insert(i);
		}
		
		Collection<Item> fromDB = repo.getAllWithPriceSorting("DESC");
		assertThat("Exactly one item was returned + 5 prepared", fromDB.size(),is((int)id+5));	
		
		long lastprice=1000;
		for (Item elementFromDB : fromDB)
		{
			assertThat("price = price", elementFromDB.getPrice() <= lastprice);
			lastprice = elementFromDB.getPrice();
		}
	}
	
	
	@ParameterizedTest
	@CsvSource({"1,One,15,800,50,6166,Bahnhofstr. 3","2,Two,17,650,48,6162,Brückfeldstrasse 41","3,Three,15,700,55,6210,Römerweg 5"})
	void testGetAllWithPriceSorting_getAllItems_sortedASC(long id,String name,int quantity, long price, int flatSize, int plz, String street)
	{
		for(int n = 0; n < id; ++n) {
			Item i = new Item(0, name, quantity, price, flatSize, plz, street);
			repo.insert(i);
		}
		
		Collection<Item> fromDB = repo.getAllWithPriceSorting("ASC");
		assertThat("Exactly one item was returned + 5 prepared", fromDB.size(),is((int)id+5));	
		
		long lastprice=0;
		for (Item elementFromDB : fromDB)
		{
			assertThat("price = price", elementFromDB.getPrice() >= lastprice);
			lastprice = Long.valueOf(elementFromDB.getPrice());
		}
	}

	
	@ParameterizedTest
	@CsvSource({"1,One,15,480,50,6166,Bahnhofstr. 3"})
	void testGetByPriceAndDescription_noRecordfound(long id,String name,int quantity, long price, int flatSize, int plz, String street)
	{
		Item i = new Item(0, name, quantity, price, flatSize, plz, street);
		repo.insert(i);

		Collection<Item> fromDB = repo.getByPriceAndDescription(String.valueOf(450),name);
		assertThat("Exactly no item was returned", fromDB.size(),is((int)0));
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,15,490,50,6166,Bahnhofstr. 3"})
	void testGetByPriceAndDescription_oneRecord_found(long id,String name,int quantity, long price, int flatSize, int plz, String street)
	{

		Item i = new Item(0, name, quantity, price, flatSize, plz, street);
		repo.insert(i);
		
		
		Collection<Item> fromDB = repo.getByPriceAndDescription(String.valueOf(500),name);
		assertThat("Exactly one item was returned", fromDB.size(),is((int)1));
		
		for(Item elementFromDB : fromDB)
		{
			assertThat("name = name", elementFromDB.getName(),is(name) );
			assertThat("quantity = quantity", elementFromDB.getQuantity(),is(quantity) );
			assertThat("price = price", elementFromDB.getPrice(),is(price) );
			assertThat("flatSize = flatSize", elementFromDB.getFlatSize(),is(flatSize) );
			assertThat("plz = plz", elementFromDB.getPlz(),is(plz) );
			assertThat("street = street", elementFromDB.getStreet(),is(street) );
		}
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,15,480,50,6166,Bahnhofstr. 3"})
	void testGetByPriceAndDescriptionWithPriceSorting_noRecordfound(long id,String name,int quantity, long price, int flatSize, int plz, String street)
	{
		Item i = new Item(0, name, quantity, price, flatSize, plz, street);
		repo.insert(i);

		Collection<Item> fromDB = repo.getByPriceAndDescriptionWithPriceSorting(String.valueOf(450),name,"DESC");
		assertThat("Exactly no item was returned", fromDB.size(),is((int)0));
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,15,490,50,6166,Bahnhofstr. 3"})
	void testGetByPriceAndDescriptionWithPriceSorting_oneRecord_found(long id,String name,int quantity, long price, int flatSize, int plz, String street)
	{

		Item i = new Item(0, name, quantity, price, flatSize, plz, street);
		repo.insert(i);
		
		
		Collection<Item> fromDB = repo.getByPriceAndDescriptionWithPriceSorting(String.valueOf(500),name,"DESC");
		assertThat("Exactly one item was returned", fromDB.size(),is((int)1));
		
		for(Item elementFromDB : fromDB)
		{
			assertThat("name = name", elementFromDB.getName(),is(name) );
			assertThat("quantity = quantity", elementFromDB.getQuantity(),is(quantity) );
			assertThat("price = price", elementFromDB.getPrice(),is(price) );
			assertThat("flatSize = flatSize", elementFromDB.getFlatSize(),is(flatSize) );
			assertThat("plz = plz", elementFromDB.getPlz(),is(plz) );
			assertThat("street = street", elementFromDB.getStreet(),is(street) );
		}
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,15,480,50,6166,Bahnhofstr. 3"})
	void testGetByPriceWithPriceSorting_noRecordfound(long id,String name,int quantity, long price, int flatSize, int plz, String street)
	{
		Item i = new Item(0, name, quantity, price, flatSize, plz, street);
		repo.insert(i);

		Collection<Item> fromDB = repo.getByPriceWithPriceSorting(String.valueOf(450),"DESC");
		assertThat("Exactly no item was returned", fromDB.size(),is((int)0));
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,15,490,50,6166,Bahnhofstr. 3"})
	void testGetByPriceWithPriceSorting_oneRecord_found(long id,String name,int quantity, long price, int flatSize, int plz, String street)
	{

		Item i = new Item(0, name, quantity, price, flatSize, plz, street);
		repo.insert(i);
		
		
		Collection<Item> fromDB = repo.getByPriceWithPriceSorting(String.valueOf(490),"DESC");
		assertThat("Exactly one item was returned", fromDB.size(),is((int)1));
		
		for(Item elementFromDB : fromDB)
		{
			assertThat("name = name", elementFromDB.getName(),is(name) );
			assertThat("quantity = quantity", elementFromDB.getQuantity(),is(quantity) );
			assertThat("price = price", elementFromDB.getPrice(),is(price) );
			assertThat("flatSize = flatSize", elementFromDB.getFlatSize(),is(flatSize) );
			assertThat("plz = plz", elementFromDB.getPlz(),is(plz) );
			assertThat("street = street", elementFromDB.getStreet(),is(street) );
		}
	}
	
	
	@ParameterizedTest
	@CsvSource({"1,One,15,480,50,6166,Bahnhofstr. 3"})
	void testGetByNameWithPriceSorting_noRecordfound(long id,String name,int quantity, long price, int flatSize, int plz, String street)
	{
		Item i = new Item(0, name, quantity, price, flatSize, plz, street);
		repo.insert(i);

		Collection<Item> fromDB = repo.getByNameWithPriceSorting("TestWillNotBeFound","DESC");
		assertThat("Exactly no item was returned", fromDB.size(),is((int)0));
	}
	
	@ParameterizedTest
	@CsvSource({"1,One,15,490,50,6166,Bahnhofstr. 3"})
	void testGetByNameWithPriceSorting_oneRecord_found(long id,String name,int quantity, long price, int flatSize, int plz, String street)
	{

		Item i = new Item(0, name, quantity, price, flatSize, plz, street);
		repo.insert(i);
		
		
		Collection<Item> fromDB = repo.getByNameWithPriceSorting(name,"DESC");
		assertThat("Exactly one item was returned", fromDB.size(),is((int)1));
		
		for(Item elementFromDB : fromDB)
		{
			assertThat("name = name", elementFromDB.getName(),is(name) );
			assertThat("quantity = quantity", elementFromDB.getQuantity(),is(quantity) );
			assertThat("price = price", elementFromDB.getPrice(),is(price) );
			assertThat("flatSize = flatSize", elementFromDB.getFlatSize(),is(flatSize) );
			assertThat("plz = plz", elementFromDB.getPlz(),is(plz) );
			assertThat("street = street", elementFromDB.getStreet(),is(street) );
		}
	}
}
